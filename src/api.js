const CURRENCY = 'EUR';
const AGGREGATE_INDEX = "5";
const INVALID_COIN_TYPE = "500";
const API_KEY = 'bc0e0414406ba277576331602b2c8a85ffeb3917e71fc37abe5d83d1eb7c60c2';
const LOAD_CURRENCY_URL = 'https://min-api.cryptocompare.com/data/all/coinlist';

const tickersHandlers = new Map();
const socket = new WebSocket(
    `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
);

let lastPriceBTC = 0;
let crossSubscribed = [];

socket.addEventListener("message", e => {
    let valid = true;
    let { TYPE: type, FROMSYMBOL: ticker, PRICE: newPrice, PARAMETER: parameter } = JSON.parse(
        e.data
    );
    if (ticker === 'BTC' && lastPriceBTC !== newPrice){
        lastPriceBTC = newPrice;
    }
    //Count amount to EUR if currency subscribes by BTC
    if(crossSubscribed.includes(ticker)){
        newPrice *= lastPriceBTC;
    }else if(type === INVALID_COIN_TYPE){
        valid = false;
        newPrice = '-';
        ticker = parameter.substring(9, parameter.length - 4);
        if (tickersHandlers.get(ticker)){
            crossSubscribe(ticker);
        }
    }else if ((type !== AGGREGATE_INDEX || newPrice === undefined) && !crossSubscribed.includes(ticker)) {
        return;
    }

    const handlers = tickersHandlers.get(ticker) ?? [];
    handlers.forEach(fn => fn(newPrice, valid));
});
const crossSubscribe = ticker => {
    if (!crossSubscribed.includes(ticker)){
        crossSubscribed.push(ticker);
        subscribeToTickerOnWs(ticker, 'BTC');
    }
}
const sendToWebSocket = message => {
    const stringifiedMessage = JSON.stringify(message);
    if (socket.readyState === WebSocket.OPEN) {
        socket.send(stringifiedMessage);
        return;
    }
    socket.addEventListener("open", () => {
            socket.send(stringifiedMessage);
        }, { once: true });
}
const subscribeToTickerOnWs = (ticker, currency = '') => {
    currency = (currency === '') ? CURRENCY : currency;

    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${ticker}~${currency}`]
    });
}
const unsubscribeFromTickerOnWs = ticker => {
    if (crossSubscribed.includes(ticker)){
        const key = Object.keys(crossSubscribed).find(index => crossSubscribed[index] === ticker);
        crossSubscribed.splice(parseInt(key), 1);
        sendToWebSocket({
            action: "SubRemove",
            subs: [`5~CCCAGG~${ticker}~BTC`]
        });
    }

    sendToWebSocket({
        action: "SubRemove",
        subs: [`5~CCCAGG~${ticker}~${CURRENCY}`]
    });
}
export const subscribeToTicker = (ticker, cb) => {
    appendTickerHandler(ticker, cb);
    subscribeToTickerOnWs(ticker);
};
export const unsubscribeFromTicker = ticker => {
    tickersHandlers.delete(ticker);
    unsubscribeFromTickerOnWs(ticker);
};
const appendTickerHandler = (ticker, fn) => {
    const subscribers = tickersHandlers.get(ticker) || [];
    tickersHandlers.set(ticker, [...subscribers, fn]);
}
export const loadExistingTickersList = () => {
    return fetch(
        `${LOAD_CURRENCY_URL}?api_key=${API_KEY}`
    )
        .then(r => r.json())
        .then(rawData => {
            if (rawData.HasWarning){
                return [];
            }

            return Object.keys(rawData.Data);
        });
};
